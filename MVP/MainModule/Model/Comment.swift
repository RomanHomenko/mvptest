//
//  Comment.swift
//  MVP
//
//  Created by Роман Хоменко on 03.03.2022.
//

import Foundation

struct Comment: Decodable {
    var postId: Int
    var id: Int
    var name: String
    var email: String
    var body: String
}
