//
//  DetailViewController.swift
//  MVP
//
//  Created by Роман Хоменко on 04.03.2022.
//

import UIKit

class DetailViewController: UIViewController {
    // MARK: - IBOutlet
    @IBOutlet weak var commentLabel: UILabel!
    
    var presenter: DetailViewPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.setComment()
    }
    
    // MARK: - IBAction
    @IBAction func popToRootVCButton(_ sender: Any) {
        presenter.tap()
    }
    
}

extension DetailViewController: DetailViewProtocol {
    func setCOmment(comment: Comment?) {
        commentLabel.text = comment?.body
    }
}
